from properties_loader import PropertiesClassLoader


class AutoLoad(PropertiesClassLoader):

    def __init__(self):
        super(AutoLoad, self).__init__(groups=['INFO'])

if __name__ == '__main__':
    auto_load: AutoLoad = AutoLoad()
    print(auto_load._INFO)